#include <ostream>
#include <Windows.h>
#include "Helper.h"
#include <string>

std::string ExePath();
void setPath(std::string path);
void createNewFIle(std::string file);
void listFiles(std::string szDir);
void secret();
void runExe(std::string file);

using namespace std;

int main()
{
	int i;
	std::vector<std::string> commands;
	std::string command;
	do {
		std::cout << ">>";
		std::getline(std::cin, command);
	} while (command.size() == 0);

	commands = Helper::get_words(command);
	for (i = 0; i < commands.size(); i++)
	{
		Helper::trim(commands[i]);
	}
	
	while (strcmp(commands[0].c_str(), "exit"))
	{

		if (!strcmp(commands[0].c_str(), "pwd"))
		{
			std::cout << ExePath() << std::endl;
		}
		else if (!strcmp(commands[0].c_str(), "cd"))
		{
			if (commands.size() == 2)
				setPath(commands[1]);
			else
				std::cout << "Wrong parameter number" << std::endl << "Expected : 1 Actual : "<< commands.size()-1 << endl;
		}
		else if (!strcmp(commands[0].c_str(), "create"))
		{
			if (commands.size() == 2)
				createNewFIle(commands[1]);
			else
				std::cout << "Wrong parameter number" << std::endl << "Expected : 1 Actual : " << commands.size()-1 << endl;
		}
		else if (!strcmp(commands[0].c_str(), "ls"))
		{
			if (commands.size() == 2)
				listFiles(commands[1]);
			else if
				(commands.size() == 1)
				listFiles("");
			else
				std::cout << "Wrong parameter number" << std::endl << "Expected : 1 / 2 Actual : " << commands.size()-1 << endl;
		}
		else if (!strcmp(commands[0].c_str(), "secret"))
		{
			if (commands.size() == 1)
				secret();
		else
			std::cout << "Wrong parameter number" << std::endl << "Expected : 0 Actual : " << commands.size()-1 << endl;

		}
		else if ((commands[0].find(".exe") == commands[0].length() - 4))
		{
			if (commands.size() == 1)
				runExe(commands[0]);
		}

		do {
			std::cout << ">>";
			std::getline(std::cin, command);
		} while (command.size() == 0);

		commands = Helper::get_words(command);
		for (i = 0; i < commands.size(); i++)
		{
			Helper::trim(commands[i]);
		}
	}
	return 0;

}

std::string ExePath()
{
	char oldDir[MAX_PATH];

	// get the current directory, and store it
	if (!GetCurrentDirectory(MAX_PATH, oldDir)) {
	std::cout << "Error getting current directory: " << GetLastError();
	return ""; // quit if it failed
	}
	return oldDir;
}


void setPath(std::string path)
{
	if (!SetCurrentDirectoryA(path.c_str()))
	{
		printf("Failed to change dir\nerror:%d\n", GetLastError());
		return;
	}
}

void createNewFIle(std::string file)
{
	HANDLE hfile = CreateFile(file.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!hfile)
	{
		printf("Failed to create file\nerror:%d\n", GetLastError());
		return;
	}

	if (!CloseHandle(hfile))
	{
		printf("Failed to close handle\nerror:%d\n", GetLastError());
		return;
	}
}

void listFiles(std::string szDir)
{
	HANDLE hFind;
	WIN32_FIND_DATA ffd;

	if (!strcmp(szDir.c_str(), ""))
	{
		szDir = ExePath();
	}
	szDir = szDir + "\\*";
	hFind = FindFirstFile(szDir.c_str(), &ffd);

	if (!hFind)
	{
		printf("Failed to find files\nerror:%d\n", GetLastError());
		return;
	}
	do {
		printf("%s\n", ffd.cFileName);
	} while (FindNextFileA(hFind, &ffd));

	FindClose(hFind);
}



void secret()
{
	typedef int(__cdecl *MYPROC)();
	HINSTANCE hinstLib;
	MYPROC ProcAdd;
	BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;

	hinstLib = LoadLibrary(TEXT("secret.dll"));
	if (hinstLib != NULL)
	{
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything");

		if (NULL != ProcAdd)
		{
			fRunTimeLinkSuccess = TRUE;
			cout << "TheAnswerToLifeTheUniverseAndEverything:" << ProcAdd() << endl;
		}

		fFreeResult = FreeLibrary(hinstLib);
	}

	// If unable to call the DLL function, use an alternative.
	if (!fRunTimeLinkSuccess)
		printf("Message printed from executable\n");

}

void runExe(std::string file)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));


	// Start the child process. 
	if (!CreateProcess(file.c_str(), NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

}